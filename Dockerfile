FROM ekidd/rust-musl-builder:stable as builder
COPY --chown=rust:rust . .
RUN [ "cargo", "build", "--release" ]

FROM alpine:3.15.0
RUN apk --no-cache add ca-certificates
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/rust-ci-test /usr/local/bin/lmao
CMD [ "/usr/local/bin/lmao" ]
